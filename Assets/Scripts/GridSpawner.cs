using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class GridSpawner : MonoBehaviour {
    [SerializeField] private Grid _grid;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private float _spawnSize = 5;
    private void Start() {
        for (int i = 0; i < _spawnSize; i++) {
            for (int j = 0; j < _spawnSize; j++) {
                var instantiatedObject = Instantiate(_prefab);
                var worldPosition = _grid.CellToWorld(new Vector3Int(j, 0, i));
                if (_grid.cellLayout == GridLayout.CellLayout.Hexagon) {
                    // Offset every second row
                    worldPosition.x += i % 2 == 0 ? 0.5f * _grid.cellSize.x : 0;
                }
                instantiatedObject.transform.position = worldPosition;
            }
        }
    }
}
