using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour {
    [SerializeField] private GameEvent _enterGameEvent;
    [SerializeField] private GameEvent _exitGameEvent;
    private void OnTriggerEnter(Collider other) {
        _enterGameEvent.Raise();
    }

    private void OnTriggerExit(Collider other) {
        _exitGameEvent.Raise();
    }

}
