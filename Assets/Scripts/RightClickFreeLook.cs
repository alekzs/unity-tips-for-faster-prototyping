using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class RightClickFreeLook : MonoBehaviour
{

    [SerializeField] private CinemachineFreeLook _cmFreeLook;
    [SerializeField] private float _speedX = 300;
    [SerializeField] private float _speedY = 100;

    // Update is called once per frame
    void Update()
    {
        _cmFreeLook.m_XAxis.m_MaxSpeed = Input.GetMouseButton(1) ? _speedX : 0;
        _cmFreeLook.m_YAxis.m_MaxSpeed = Input.GetMouseButton(1) ? _speedY : 0;
    }
}
