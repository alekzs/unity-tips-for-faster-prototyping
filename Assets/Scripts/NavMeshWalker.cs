using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshWalker : MonoBehaviour {

    [SerializeField] private List<NavMeshAgent> _agents;
    [SerializeField] private Camera _camera;

    // Update is called once per frame
    void Update() {
        var cameraRay = _camera.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0)) {
            if (Physics.Raycast(cameraRay, out RaycastHit info, 1000)) {
                foreach (var navMeshAgent in _agents) {
                    navMeshAgent.SetDestination(info.point);
                }
            }
        }
    }
}
