using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDriver : MonoBehaviour {
    [SerializeField] private float movementSpeed = 1.0f;
    [SerializeField] private Transform target;
    void Update() {
        Vector3 direction = (target.position - transform.position).normalized;
        Vector3 translation = direction * (movementSpeed * Time.deltaTime);
        translation.y = 0.0f;
        transform.Translate(translation);
    }
}
