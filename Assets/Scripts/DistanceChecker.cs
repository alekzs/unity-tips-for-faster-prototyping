using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class DistanceChecker : MonoBehaviour {
    [SerializeField] private Transform target;
    [SerializeField] private Transform referenceTarget;

    [SerializeField] private float maxDistance;
    [SerializeField] private float animationDuration = 3.0f;
    [SerializeField] private float animationDelay = 1.0f;
    [SerializeField] private AnimationCurve stepCurve;
    [SerializeField] private CinemachineImpulseSource impulseSource;
    private bool _isAnimating = false;
    private float _animationTimer = 0.0f;
    private float _baseTargetY = 0;
    private void Start() {
        _baseTargetY = target.position.y;
    }
    // Update is called once per frame
    void Update() {
        referenceTarget.position = new Vector3(referenceTarget.position.x, 0, referenceTarget.position.z);
        float distance = Vector3.Distance(target.position, referenceTarget.position);
        if (distance > maxDistance && !_isAnimating) {
            _isAnimating = true;
            Debug.Log("start animating");
            _animationTimer = animationDuration;
            StartCoroutine(AnimateToPosition());
        }
    }

    private IEnumerator AnimateToPosition() {
        yield return new WaitForSeconds(animationDelay);
        while (_animationTimer > 0.0f) {
            _animationTimer -= Time.deltaTime;
            float progress = Mathf.Clamp01(1.0f - (_animationTimer / animationDuration));
            float stepValue = stepCurve.Evaluate(progress);
            var position = referenceTarget.position;
            position = new Vector3(position.x, _baseTargetY + stepValue, position.z);
            target.position = Vector3.Lerp(target.position, position, progress);
            yield return null;
        }

        _isAnimating = false;
        impulseSource.GenerateImpulse();
        //impulseSource.GenerateImpulseAt(transform.position, Vector3.zero);
    }
}
