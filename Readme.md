### Unity Tips for Faster Prototyping

The project contains assets for my talk on Game Jam+ and is a collection of samples:

- Cinemachine simple follow camera
- Cinemachine free look camera
- Cinemachine target group camera with a confiner
- Cinemachine events
- Cinemachine impulse
- A general solution for using ScriptableObjects as events
- NavMesh components
- ProBuilder
- PolyBrush
- AnimationRigging Two Bone IK
- AnimationRigging Chain IK (with a procedurally animated monster, but lets keep it a secret)
- AnimationRigging Damped Transform
- Timeline + Cinemachine sample

The project also includes samples for some of the packages listed above.

Other assets used:

- Unity Starter Assets - [Third Person Character Controller](https://assetstore.unity.com/packages/essentials/starter-assets-third-person-character-controller-196526)
- ScriptableObjects as GameEvents - [Source for GameEvent and GameEventListener](https://unity.com/how-to/architect-game-code-scriptable-objects#code-example-gameevent-scriptableobject)
- NavMeshComponents - Added by name via PackageManager - [Instructions](https://docs.unity3d.com/Packages/com.unity.ai.navigation@1.0/manual/index.html)